# HTML Public Templates
---
This is just a place to share .html templates to people on the AppSheet community and also to test how Paged Media Module can make a big difference on reports made through AppSheet's automation

Please post your need on [this post](https://www.googlecloudcommunity.com/gc/AppSheet-Q-A/I-m-looking-for-dificult-template-use-cases/m-p/407836/highlight/true#M160278) so that I can help you solve it and give a good template for it.

In case you want to DM me, please [go to the AppSheet Community](https://www.googlecloudcommunity.com/gc/user/viewprofilepage/user-id/316384)
